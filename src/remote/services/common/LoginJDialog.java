package remote.services.common;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import remote.services.impl.main.Cliente;
import remote.services.impl.main.Repositorio;
import remote.services.impl.test.ConstantLocalhost;

public class LoginJDialog extends JDialog {

	private JTextField tfUsername;
	private JPasswordField pfPassword;
	private JLabel lbUsername;
	private JLabel lbPassword;
	private JButton btnLogin;
	private JButton btnExit;
	private JButton btnReg;
	private int session = 0;
	private ServicioRegistroInterface registrer;
	private ServicioAutenticacionInterface autentication;
	private JFrame mainwindow;
	
	public LoginJDialog(JFrame mainwindow, int TYPE) {
		super(mainwindow,"Login",true);
		this.mainwindow = mainwindow;
		//Construimos la ventana
		JPanel contentPanel = new JPanel(new GridBagLayout());
		GridBagConstraints cs = new GridBagConstraints();
		cs.fill = GridBagConstraints.HORIZONTAL;
		//Construimos los campos
        lbUsername = new JLabel("Usuario: ");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        contentPanel.add(lbUsername, cs);
 
        tfUsername = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 2;
        contentPanel.add(tfUsername, cs);
 
        lbPassword = new JLabel("Contraseña: ");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        contentPanel.add(lbPassword, cs);
        
        pfPassword = new JPasswordField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        contentPanel.add(pfPassword, cs);
        
		//Botones
        btnInitSession(TYPE);
        btnReg(TYPE);
        btnExit();
        
        JPanel bp = new JPanel();
        bp.add(btnLogin);
        bp.add(btnReg);
        bp.add(btnExit);
 
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);
 
        pack();
        setResizable(false);
        setLocationRelativeTo(mainwindow);
	}
	
	private void btnInitSession(int USER_TYPE) {
		btnLogin= new JButton("Iniciar sesión.");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					autentication = (ServicioAutenticacionInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR  + "/autentication");
					System.out.println("servicio encontrado");
					//Datos de lso campos
					String user = tfUsername.getText();
					//String pass = MD5Pass.hashValue(pfPassword.getPassword());
					String pass = pfPassword.getPassword().toString();
					//JOptionPane.showMessageDialog(null, "Pass: " + pass,"LOGIN",JOptionPane.INFORMATION_MESSAGE);
					//segun si es usuario o repo
					if( USER_TYPE == 0) {
						session = autentication.validateRepositorie(user,pass);
					}
						
					else {
						session = autentication.validateClient(user, pass);
					}
					
					if(session != 0 && mainwindow instanceof Repositorio) {
						((Repositorio) mainwindow).setSession(session);
						mainwindow.setVisible(true);
						dispose();
					}else if(session != 0 && mainwindow instanceof Cliente) {
						((Cliente) mainwindow).setSession(session);
						mainwindow.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "El usuario no se encuentra registrado o la pass es incorrecta.","LOGIN",JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				} catch (RemoteException e1) {
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					e1.printStackTrace();
				/*} catch (NoSuchAlgorithmException e1) {
					e1.printStackTrace();*/
				}
			}
		});	
	}
	
	private void btnReg(int USER_TYPE) {
		btnReg = new JButton("Registrar");
		btnReg.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					registrer = (ServicioRegistroInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/reg");
					//Recogemos campos del formulario
					String name = tfUsername.getText();
					//String pass = MD5Pass.hashValue(pfPassword.getPassword());
					String pass = pfPassword.getPassword().toString();
					//JOptionPane.showMessageDialog(null, "La pass en registro:  " + pass,"LOGIN",JOptionPane.INFORMATION_MESSAGE);
					boolean sucefullReg = false;
					
					if(USER_TYPE == 0) {
						System.out.println("Intentado registrar nuevo repositorio..");
						sucefullReg = registrer.regRepository(name, pass);
					}
					else {
						System.out.println("Intentado registrar nuevo cliente..");
						sucefullReg = registrer.regClient(name, pass);
					}
						
					
					//Comprobamos si se registro existosamente
					if( sucefullReg == true) {
						JOptionPane.showMessageDialog(null,"Usuario registrado con éxito.","REGISTRO",JOptionPane.INFORMATION_MESSAGE);
					}else{
						tfUsername.setText("");
						pfPassword.setText("");
						JOptionPane.showMessageDialog(null, "El usuario ya se encuentra registrado.","REGISTRO",JOptionPane.INFORMATION_MESSAGE);
					}
					
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				} catch (RemoteException e2) {
					e2.printStackTrace();
				} catch (NotBoundException e3) {
					e3.printStackTrace();
				/*} catch (NoSuchAlgorithmException e4) {
					e4.printStackTrace();*/
				}
			}
		});
		
	}
	private void btnExit() {
		btnExit = new JButton("Salir");
		btnExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
	}
	
	public int getSession() {
		return session;
	}
}
