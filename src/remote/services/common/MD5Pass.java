package remote.services.common;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Pass {

	//APLICA EL ALGORITMO MD5 A LA CONTRASEÑA Y LA CONVIERTE EN UNA CADENA DE 32 CARACTERES
	public static String hashValue(char[] pass) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] passDigest = md.digest(pass.toString().getBytes());
		
		BigInteger bigInt = new BigInteger(1,passDigest);
		String hashPass= bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		while(hashPass.length() < 32 ){
			hashPass = "0"+hashPass;
		}
		return hashPass;
	}
	
	
}
