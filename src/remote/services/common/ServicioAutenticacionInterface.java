package remote.services.common;

import java.rmi.RemoteException;

/**
 * Se encarga de registrar y de autenticar, cuando sea
 * necesario, las otras entidades participantes en el sistema: clientes y repositorios.
 * Ambas entidades se tienen que dar de alta en el sistema y recibir un identificador
 * único para poder operar y realizar almacenaje de ficheros. El registro se lleva a
 * cabo cuando este servicio devuelve a la entidad demandante (cliente o
 * repositorio) el identificador único con el que tiene que autenticarse para
 * cualquier operación que lo requiera en el sistema.
 * @author ibernabeu
 *
 */
public interface ServicioAutenticacionInterface extends java.rmi.Remote{
	
	int validateClient(String name, String pass) throws RemoteException;
	
	int validateRepositorie(String name, String pass) throws RemoteException;
	
	void disconect(int session, int USER_TYPE) throws RemoteException;
}
 