package remote.services.common;

import java.rmi.RemoteException;

/**
 * Este servicio se encarga de las operaciones de subida
de ficheros al repositorio y borrado de los mismos. El servicio Gestor del
servidor responde a la petición del cliente enviándole la URL de este servicio
para que pueda completar su operación
 * @author ibernabeu
 *
 */
public interface ServicioClOperadorInterface extends java.rmi.Remote{

	boolean uploadFile(Fichero file) throws RemoteException;
	
	boolean deleteFile(String fileName, int idClient) throws RemoteException;
	
}
