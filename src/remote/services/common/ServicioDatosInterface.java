package remote.services.common;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;

/**
 * Este servicio hará las funciones de una base de datos que relacione
 * Clientes-Ficheros-Metadatos-Repositorios. Es decir, mantendrá lista de clientes
 * y repositorios conectados al sistema, junto con los ficheros; y los relacionarán
 * permitiendo operaciones de consulta, borrado y añadido. Los dos servicios
 * anteriores (Servicio Autenticación y Servicio Gestor) harán uso de este servicio
 * para realizar las operaciones sobre el estado de las entidades del sistema y sus
 * atributos. Aunque podría usarse un sistema de gestión de bases de datos (SGBD)
 * para implementar este servicio, esto haría muy complejo el desarrollo de la
 * práctica y no atendería a los objetivos de la asignatura. Así pues, el equipo
 * docente recomienda para la implementación del servicio las clases List y HashMap de Java
 * @author ibernabeu
 *
 */
public interface ServicioDatosInterface extends java.rmi.Remote {
	
	final int USER_RESPOSITORY  = 0;
	
	final int USER_CLIENT = 1;
	
	boolean registrer(String user, String pass, int USER_TYPE) throws RemoteException;

	boolean connect(int session, String user, String pass, int USER_TYPE) throws RemoteException;
	
	void disconect(int session, int USER_TYPE) throws RemoteException;
	
	Collection<?> getClientSessions() throws RemoteException;
	
	Collection<?> getRepositoriesSessions() throws RemoteException;
	
	Collection<?> getClients() throws RemoteException;
	
	Collection<?> getRepositories() throws RemoteException;
	
	Collection<?> getMetadataInfo() throws RemoteException;
	
	Map<?,?> getClientsRepositoriesAsociate() throws RemoteException;
	
	int searchRepoOfClient(int idClient) throws RemoteException;
	
	void updateMetadata(int idRepo, int idCliente, String newfile, int OPTION) throws RemoteException;

	
}
