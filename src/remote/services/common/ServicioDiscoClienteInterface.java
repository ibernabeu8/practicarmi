package remote.services.common;

import java.rmi.RemoteException;

/**
 * El cliente publica la interfaz de un servicio cuyo nombre es DiscoCliente que será
	utilizado por el servicio Servidor-Operador del repositorio para descargar al disco duro
	local del cliente el fichero que este considere oportuno
 * @author ibernabeu
 *
 */
public interface ServicioDiscoClienteInterface extends java.rmi.Remote {
	
	void downloadFileFromRepository(Fichero file) throws RemoteException;
}
