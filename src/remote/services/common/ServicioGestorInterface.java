package remote.services.common;

import java.rmi.RemoteException;

/**
 * Este servicio se encarga de gestionar las operaciones de los
 * clientes en relación a sus ficheros en la nube (físicamente alojados en los
 * repositorios). Usando este servicio el cliente podrá subir, bajar y borrar ficheros
 * en la nube, además de listar sus ficheros almacenados y compartir ficheros con
 * otros clientes que estén registrados en el sistema.
 * @author ibernabeu
 *
 */
public interface ServicioGestorInterface extends java.rmi.Remote{
	
	String getURLClientOperator(int id) throws RemoteException;
	
	String getURLServerOperator(int id) throws RemoteException;
	
}