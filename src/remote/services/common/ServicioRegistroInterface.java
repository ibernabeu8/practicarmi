package remote.services.common;

import java.rmi.RemoteException;

public interface ServicioRegistroInterface extends java.rmi.Remote{

	boolean regClient(String name, String pass) throws RemoteException;
	
	boolean regRepository(String name, String pass) throws RemoteException;
}
