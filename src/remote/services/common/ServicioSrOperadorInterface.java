package remote.services.common;

import java.rmi.RemoteException;
import java.util.Collection;

import remote.services.impl.server.Metadata;

/**
 * Este servicio tiene un doble objetivo. Por un lado,
suministra los métodos necesarios para que el servidor gestione los lugares de
almacenamiento para cada cliente, y por otro lado se encarga de la 


DESCAR REPOSITORIO - CLIENTE
1. Cliente llama al servidor (GESTOR)
2. Servidor(GESTOR) averigua repositorio y devuelve URL(del cliente) HACIA ESTE SERVICIO
3.

operación de bajada de ficheros desde el repositorio al cliente, es decir, cuando un cliente
quiere bajar un fichero se lo pide al servidor mediante el servicio Gestor. 
Una vez que el servidor averigua que repositorio aloja el fichero requerido por el
cliente, éste llama a un método del Servicio Servidor-Operador y le pasa la URL
del cliente para que pueda llamar al método de descarga del servicio
DiscoCliente que es el que realmente se encarga de la descarga. 

De esta manera
no cargamos al servidor con esta operación temporalmente costosa en términos
de entrada/salida y conseguimos Escalabilidad.
 * @author ibernabeu
 *
 */
public interface ServicioSrOperadorInterface extends java.rmi.Remote{
	
	static int DELETE = 0;
	
	static int CREATE = 1;
	
	void newRepoClient(int idClient) throws RemoteException;
	
	void downloadFile(String sessionClient, String fileName) throws RemoteException;
	
	Collection<Fichero> getFiles(int session) throws RemoteException;
	
	Collection<Metadata> getFilesFromClient(int idClient) throws RemoteException;
	
	void updateFilesList(int idClient, String fileName, int OPTION) throws RemoteException;
}
