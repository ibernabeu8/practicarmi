package remote.services.common;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class StartRegistry {
	public static void start (int numRMIport) throws RemoteException {
			try {
				Registry registry = LocateRegistry.getRegistry(numRMIport);
				registry.list();
				// El método anterior lanza una excepción
				// si el registro no existe.
			}
			catch (RemoteException exc) {
				//No existe un registro válido en este puerto.
				System.out.println("El registro RMI no se puede localizar en el puerto: "+ numRMIport);
				LocateRegistry.createRegistry(numRMIport);
				System.out.println("Registro RMI creado en el puerto " + numRMIport);
			} // fin catch
	} // fin arrancarRegistro
}
			
