package remote.services.impl.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import remote.services.common.Fichero;
import remote.services.common.ServicioDiscoClienteInterface;
import remote.services.impl.repository.ServicioClOperadorImpl;

public class ServicioDiscoClienteImpl extends UnicastRemoteObject implements ServicioDiscoClienteInterface {

	public ServicioDiscoClienteImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	//el fichero contiene ya la ruta donde se encuentra ubicado
	@Override
	public void downloadFileFromRepository(Fichero file) throws RemoteException {
		String destiny = ClassLoader.getSystemClassLoader().getResource(".").getPath() + File.separator + "Downloads" + File.separator;
		
		File f = new File(destiny);
		if(!f.exists()) {
			f.mkdir();
		}

		OutputStream os;
		try {
			os = new FileOutputStream(destiny + File.separator + file.obtenerNombre());
			file.escribirEn(os);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private boolean newFolder(String path) {
		System.out.println("Creando directorio Descargas");
		File f = new File(path);	
		if(!f.exists()) {
			return f.mkdir();
		}
		System.out.println("Ya existe.");
		return false;
	}
}
