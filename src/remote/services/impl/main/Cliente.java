package remote.services.impl.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.omg.Messaging.SyncScopeHelper;

import remote.services.common.Fichero;
import remote.services.common.LoginJDialog;
import remote.services.common.ServicioAutenticacionInterface;
import remote.services.common.ServicioClOperadorInterface;
import remote.services.common.ServicioGestorInterface;
import remote.services.common.ServicioSrOperadorInterface;
import remote.services.common.StartRegistry;
import remote.services.impl.client.ServicioDiscoClienteImpl;
import remote.services.impl.server.Client;
import remote.services.impl.server.Metadata;
import remote.services.impl.test.ConstantLocalhost;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTable;


public class Cliente extends JFrame {


	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JFileChooser filechooser;
	private JList<Fichero> jlistFiles;
	//servicios remotos
	private ServicioGestorInterface manager;
	private ServicioSrOperadorInterface sroperador;

	private JButton btnUploadFile,btnDownloadFile,btnDeleteFile,btnListFiles,btnExit;
	
	private ServicioDiscoClienteImpl diskClient;
	private int id;
	private int session;
	private JLabel iconLabel;
	private JTable table;
	
	public static void main(String[] args) {
		Cliente clientMain = new Cliente();
		LoginJDialog jd = new LoginJDialog(clientMain,1);
		clientMain.setVisible(false);
		jd.setVisible(true);
	}

	public Cliente() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 498, 436);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 0, 249, 217);
		contentPane.add(panel);
		
		btnUploadFile();
		
		//btnDownloadFile();
		btnDownloadFile();
		btnDeleteFile();
		
		try {
			btnListFiles();
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		btnExit();
		JButton btnSharedFile = new JButton("NO IMPLEMENTADO");
		
		
		JButton btnListOtherClients = new JButton("Listar clientes del sistema");
		
		
		
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel.add(btnUploadFile);
		panel.add(btnDownloadFile);
		panel.add(btnDeleteFile);
		//panel.add(btnSharedFile);
		panel.add(btnListFiles);
		//panel.add(btnListOtherClients);
		panel.add(btnExit);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 229, 461, 164);
		contentPane.add(scrollPane);
		
		
		scrollPane.setViewportView(jlistFiles);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		iconLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/cliente_icon.png")).getImage();
		iconLabel.setIcon(new ImageIcon(img));
		iconLabel.setBounds(271, 0, 202, 205);
		contentPane.add(iconLabel);
		/*
		1.- Subir fichero.
		2.- Bajar fichero.
		3.- Borrar fichero.
		4.- Compartir fichero (Opcional).
		5.- Listar ficheros.
		6.- Listar Clientes del sistema.
		7.- Salir.
		*/
	}
	
	private void init() {
		try {
			System.out.println("Cliente: " + session);
			StartRegistry.start(ConstantLocalhost.PUERTO_CLIENTE);
			diskClient = new ServicioDiscoClienteImpl();
			Naming.rebind("rmi://localhost:" + ConstantLocalhost.PUERTO_CLIENTE + "/clientdisk/" + session, diskClient);
			//buscamos los servicios que usara
			manager = (ServicioGestorInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/manager");

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setSession(int session) {
		this.session = session;
		init();
	}
	
	private void btnUploadFile() {
		btnUploadFile = new JButton("Subir fichero");
		btnUploadFile.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				File file = JFileChooser();
				String path = file.getPath();
				String name = file.getName();
				
				try {
					//Cl - operator, viene con el ID del cliente separado de ID - url
					String[] urlIDRepoClOp = manager.getURLClientOperator(session).split("-");
					String idClient = urlIDRepoClOp[0];
					String urlRepoClOp = urlIDRepoClOp[1];
					ServicioClOperadorInterface cloperator = (ServicioClOperadorInterface)Naming.lookup(urlRepoClOp);
					Fichero fichero = new Fichero(path,name,idClient);
					cloperator.uploadFile(fichero);
					//Sr para actualizar si la subida es exitosa
					String[] urlIDRepoSrOp = manager.getURLServerOperator(session).split("-");
					String urlRepoSrOp = urlIDRepoSrOp[1];
					ServicioSrOperadorInterface sroperator = (ServicioSrOperadorInterface)Naming.lookup(urlRepoSrOp);
					sroperator.updateFilesList(Integer.valueOf(idClient), name, sroperator.CREATE);
				} catch (RemoteException e1) {
					
					e1.printStackTrace();
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
	}
	
	private void btnDeleteFile() {
		btnDeleteFile = new JButton("Borrar fichero.");
		btnDeleteFile.setEnabled(false);
		btnDeleteFile.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					//Cl - operator, viene con el ID del cliente separado de ID - url
					String[] urlIDRepoClOp = manager.getURLClientOperator(session).split("-");
					int idClient = Integer.valueOf(urlIDRepoClOp[0]);
					String urlRepoClOp = urlIDRepoClOp[1];
					ServicioClOperadorInterface cloperator = (ServicioClOperadorInterface)Naming.lookup(urlRepoClOp);
					String fileName = (String) table.getModel().getValueAt(table.getSelectedRow(), 1);
					cloperator.deleteFile(fileName, idClient);
					//Actualizamos la bbdd
					String[]urlIDRepoSrOp = manager.getURLServerOperator(session).split("-");
					ServicioSrOperadorInterface sroperator = (ServicioSrOperadorInterface)Naming.lookup(urlIDRepoSrOp[1]);
					sroperator.updateFilesList(idClient, fileName, sroperator.DELETE);
					showFilesInTable();
				} catch (RemoteException e1) {
					e1.printStackTrace();
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
	
	private void btnDownloadFile() {
		btnDownloadFile = new JButton("Descargar fichero");
		btnDownloadFile.setEnabled(false);
		btnDownloadFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				//llamamos a srop
				String[] urlIDRepoSrOp;
				try {
					urlIDRepoSrOp = manager.getURLServerOperator(session).split("-");
					String urlRepoSrOp = urlIDRepoSrOp[1];
					ServicioSrOperadorInterface sroperator = (ServicioSrOperadorInterface)Naming.lookup(urlRepoSrOp);
					String fileName = (String) table.getModel().getValueAt(table.getSelectedRow(), 1);
					sroperator.downloadFile(String.valueOf(session),fileName);
				} catch (RemoteException e) {
					e.printStackTrace();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (NotBoundException e) {
					e.printStackTrace();
				}

			}
		});
	}
	


	private void btnListFiles() throws MalformedURLException, RemoteException, NotBoundException {
		btnListFiles = new JButton("Listar ficheros");
		btnListFiles.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
					try {
						showFilesInTable();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		});
	}
	
	
	private void showFilesInTable() throws RemoteException, MalformedURLException, NotBoundException {
		String urlRepoSrOp = manager.getURLServerOperator(session);	
		String[] url = urlRepoSrOp.split("-");
		int idClient = Integer.valueOf(url[0]);
		ServicioSrOperadorInterface srop = (ServicioSrOperadorInterface) Naming.lookup(url[1]);
		Collection<Metadata> list = srop.getFilesFromClient(idClient);
		
		String[] columnNames = {"FECHA","Nombre"};
		DefaultTableModel model = new DefaultTableModel(new Object[0][0],columnNames);
		
		for(Metadata m : list) {
			Object[] row = new Object[2];
			row[0] = m.getDate();
			row[1] = m.getFileName();
			model.addRow(row);
		}
		
		model.addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				// TODO Auto-generated method stub
				System.out.println("test");
			}
		});
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				btnDownloadFile.setEnabled(true);
				btnDeleteFile.setEnabled(true);
			}
		});
		
		table.setModel(model);
		table.updateUI();
	}

	 

	
	private void btnExit() {
		btnExit = new JButton("Salir");
		btnExit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Naming.unbind("rmi://localhost:" + ConstantLocalhost.PUERTO_CLIENTE + "/clientdisk/" + session);
					ServicioAutenticacionInterface autenticacion = (ServicioAutenticacionInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/autentication");
					autenticacion.disconect(session, 1);
					System.exit(0);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					e1.printStackTrace();
				}
				//buscamos los servicios que usara
			}
		});
	}
	private File JFileChooser() {
		JButton select = new JButton("Seleccionar");
		filechooser = new JFileChooser();
		filechooser.setCurrentDirectory(new java.io.File("."));
		filechooser.setDialogTitle("Subir fichero.");
		filechooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
		
		if(filechooser.showOpenDialog(select) == filechooser.APPROVE_OPTION) {
			
		}
		return filechooser.getSelectedFile();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
