package remote.services.impl.main;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import remote.services.common.Fichero;
import remote.services.common.LoginJDialog;
import remote.services.common.ServicioAutenticacionInterface;
import remote.services.common.ServicioDatosInterface;
import remote.services.common.StartRegistry;
import remote.services.impl.repository.ServicioClOperadorImpl;
import remote.services.impl.repository.ServicioSrOperadorImpl;
import remote.services.impl.server.Client;
import remote.services.impl.server.Metadata;
import remote.services.impl.test.ConstantLocalhost;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Repositorio extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JList<Metadata> jlist;
	private int session;
	private JButton btnListClients,btnListFilesOfClient,btnExit;
	private ServicioSrOperadorImpl srOp;
	private ServicioClOperadorImpl clop;
	private JLabel iconLabel;
	private JScrollPane scrollPane_1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		//LoginWindow login= new LoginWindow(0); 
		//login.setVisible(true);
		Repositorio repositoryMain = new Repositorio();
		LoginJDialog jd = new LoginJDialog(repositoryMain,0);
		repositoryMain.setVisible(false);
		jd.setVisible(true);
		
	}

	/**
	 * Create the frame.
	 */
	public Repositorio() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 775, 484);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnListClients();
		btnListClients.setBounds(579, 37, 147, 25);
		contentPane.add(btnListClients);
		
		btnListFilesOfClient();
		btnListFilesOfClient.setBounds(494, 74, 232, 25);
		contentPane.add(btnListFilesOfClient);
		
		btnExit();
		btnExit.setBounds(609, 113, 117, 25);
		contentPane.add(btnExit);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 251, 681, 190);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		iconLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/repositorio_icon.png")).getImage();
		iconLabel.setIcon(new ImageIcon(img));
		iconLabel.setBounds(44, 7, 167, 242);
		contentPane.add(iconLabel);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(204, 12, 270, 222);
		contentPane.add(scrollPane_1);
		
		jlist = new JList();
		scrollPane_1.setViewportView(jlist);

	}
	
	//Este metodo es usado por el jdialog de LOGIN, cuando el login es correcto le manda la session que nos proporciona el servicio autenticador 
	//a traves de este metodo que a continuacion lanza los servicios del repositorio asociados a ese identificador
	public void setSession(int session) {
		this.session = session;
		init();
	}
	
	private void init() {
		try {
			System.out.println("Repositorio conectado con el numero: " + session);
			StartRegistry.start(ConstantLocalhost.PUERTO_REPOSITORIO);	
			clop = new ServicioClOperadorImpl();
			Naming.rebind("rmi://localhost:" + ConstantLocalhost.PUERTO_REPOSITORIO + "/clientOperator/" + session, clop);
			srOp = new ServicioSrOperadorImpl();
			Naming.rebind("rmi://localhost:" + ConstantLocalhost.PUERTO_REPOSITORIO + "/serverOperator/" + session , srOp);

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	private void btnListClients() {
		btnListClients = new JButton("Listar clientes");
		btnListClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				Collection<Client> list = srOp.listarClientes(session);
				String[] columnNames = {"ID","Nombre"};
				DefaultTableModel model = new DefaultTableModel(new Object[0][0],columnNames);
				for(Client c : list) {
					Object[] row = new Object[2];
					row[0] = c.getId();
					row[1] = c.getUser();
					model.addRow(row);
				}
				
				table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					
					@Override
					public void valueChanged(ListSelectionEvent e) {
						btnListFilesOfClient.setEnabled(true);
					}
				});
				
				table.setModel(model);
				table.updateUI();
			}
		});
	} 

	private void btnListFilesOfClient() {
		btnListFilesOfClient = new JButton("Listar ficheros del cliente");
		btnListFilesOfClient.setEnabled(false);
		btnListFilesOfClient.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if(row != -1) {
					System.out.println(table.getModel().getValueAt(row, 0));
					String idClient = String.valueOf(table.getModel().getValueAt(row, 0)); //id seleccionado de la tabla
					Collection<Metadata> list = srOp.getFilesFromClient(Integer.valueOf(idClient));
					//
					DefaultListModel<Metadata> dlm = new DefaultListModel<Metadata>();
					
					for(Metadata m : list) {
						dlm.addElement(m);
					}
					
					jlist.setModel(dlm);
				}
			}
		});
	}
	
	private void btnExit() {
		btnExit = new JButton("Salir");
		btnExit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Naming.unbind("rmi://localhost:" + ConstantLocalhost.PUERTO_REPOSITORIO + "/clientOperator/" + session);
					Naming.unbind("rmi://localhost:" + ConstantLocalhost.PUERTO_REPOSITORIO + "/serverOperator/" + session);
					ServicioAutenticacionInterface autenticacion = (ServicioAutenticacionInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/autentication");
					autenticacion.disconect(session, 0);
					System.exit(0);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					e1.printStackTrace();
				}
				//buscamos los servicios que usara
			}
		});
	}
}
