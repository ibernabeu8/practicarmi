package remote.services.impl.main;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import remote.services.common.StartRegistry;
import remote.services.impl.server.Client;
import remote.services.impl.server.Repository;
import remote.services.impl.server.ServicioAutenticacionImpl;
import remote.services.impl.server.ServicioDatosImpl;
import remote.services.impl.server.ServicioGestorImpl;
import remote.services.impl.server.ServicioRegistroImpl;
import remote.services.impl.server.SystemEntity;
import remote.services.impl.test.ConstantLocalhost;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
/**
 * 
 * @author ibernabeu
 *
 */
public class Servidor extends JFrame {

	private static final long serialVersionUID = 1L;
	private ServicioDatosImpl data;
	private ServicioRegistroImpl registrer;
	private ServicioAutenticacionImpl autentication;
	private ServicioGestorImpl manager;
	private final int PORT = ConstantLocalhost.PUERTO_SERVIDOR;

	private JPanel contentPane;
	private JButton btnListClient, btnListRepositories, btnListClientRepo,btnOnline,btnExit;
	private JScrollPane scrollPane;
	private JTable table;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Servidor frame;
		try {
			frame = new Servidor();
			frame.setVisible(true);
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	
	}

	/**
	 * Create the frame.
	 * @throws NotBoundException 
	 * @throws RemoteException 
	 */
	public Servidor() throws NotBoundException {
		init();
		
		//Evento para cerrar servicios al cerrar ventna
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e)
		    {
				btnExit.doClick();
		    }
		});
		//gin
		setBounds(100, 100, 800, 541);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		
		JLabel iconLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/server_icon.png")).getImage();
		iconLabel.setIcon(new ImageIcon(img));
		iconLabel.setBounds(27, 23, 304, 249);
		contentPane.add(iconLabel);
		
		
		
		JPanel tittle_panel = new JPanel();
		tittle_panel.setBounds(371, 25, 397, 94);
		contentPane.add(tittle_panel);
		
		JLabel lblAlmacenDeFicheros = new JLabel("Almacen de ficheros en la nube, JAVA RMI. SERVIDOR");
		lblAlmacenDeFicheros.setHorizontalAlignment(SwingConstants.CENTER);
		tittle_panel.add(lblAlmacenDeFicheros);
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBounds(371, 131, 397, 141);
		contentPane.add(buttonsPanel);
		//INICIO BOTONES
		listClientButton();
		listRepositoryButton();
		listClientRepoButton();
		listClientRepoButton();
		exitButton();
		
		btnOnline(); 
		//LAYOUT DE LOS BOTONES
		GroupLayout gl_buttonsPanel = new GroupLayout(buttonsPanel);
		gl_buttonsPanel.setHorizontalGroup(
			gl_buttonsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonsPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_buttonsPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_buttonsPanel.createSequentialGroup()
							.addComponent(btnListClient)
							.addPreferredGap(ComponentPlacement.RELATED, 170, Short.MAX_VALUE)
							.addComponent(btnExit))
						.addComponent(btnListRepositories)
						.addComponent(btnListClientRepo)
						.addComponent(btnOnline, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_buttonsPanel.setVerticalGroup(
			gl_buttonsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonsPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_buttonsPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnListClient)
						.addComponent(btnExit))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnListRepositories)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnListClientRepo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnOnline)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		buttonsPanel.setLayout(gl_buttonsPanel);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(27, 298, 741, 200);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);

	}
	
	
	private void init() throws NotBoundException {
		
		try {
			StartRegistry.start(PORT);
			//LocateRegistry.createRegistry(PORT);
			//Levantamos servicio datos
			data = new ServicioDatosImpl();
			Naming.rebind("rmi://localhost:" + PORT + "/data", data);
			//Levantamos servicio registro
			registrer = new ServicioRegistroImpl();
			Naming.rebind("rmi://localhost:" + PORT + "/reg", registrer);
			//levantamos servicio autenticacion
			autentication = new ServicioAutenticacionImpl();
			Naming.rebind("rmi://localhost:" + PORT + "/autentication", autentication);
			//levantamos servicio gestor
			manager = new ServicioGestorImpl();
			Naming.rebind("rmi://localhost:" + PORT + "/manager", manager);
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
	}
	private void listClientButton() {
		btnListClient = new JButton("Listar Clientes");
		btnListClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				Collection<Client> lista= null;
				try {
					lista =  data.getClients();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
				String[] columnNames = {"ID","Nombre"};
				DefaultTableModel model = new DefaultTableModel(new Object[0][0],columnNames);
				for(Client c : lista) {
					Object[] row = new Object[2];
					row[0] = c.getId();
					row[1] = c.getUser();
					model.addRow(row);
				}
				table.setModel(model);
				table.updateUI();
			}
		});
	}
	
	private void listRepositoryButton() {
		btnListRepositories = new JButton("Listar Repositorios");
		btnListRepositories.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Collection<Repository> lista = null;
				try {
					lista =  data.getRepositories();
					System.out.println("Numero que me esta devolviendo el servidor de datos: de repos" + lista.size());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
				String[] columnNames = {"ID","Nombre"};
				DefaultTableModel model = new DefaultTableModel(new Object[0][0],columnNames);
				for(Repository r : lista) {
					Object[] row = new Object[2];
					row[0] = r.getId();
					row[1] = r.getUser();
					model.addRow(row);
				}
				table.setModel(model);
				table.updateUI();
			}
		});
	}
	
	private void listClientRepoButton() {
		btnListClientRepo = new JButton("Listar Parejas Repositorio-Cliente");
		btnListClientRepo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Map<Client,Repository> mapList= null;
				try {
					mapList =  data.getClientsRepositoriesAsociate();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
				String[] columnNames = {"IDRepo","NombreRepo","IDcliente","NombreC"};
				DefaultTableModel model = new DefaultTableModel(new Object[0][0],columnNames);
				
				for (Map.Entry<Client, Repository> entry : mapList.entrySet())
				{
					Object[] row = new Object[4];
					row[0] = entry.getValue().getId();
					row[1] = entry.getValue().getUser();
					row[2] = entry.getKey().getId();
					row[3] = entry.getKey().getUser();
					model.addRow(row);
				}
				table.setModel(model);
				table.updateUI();
			}
		});
	}
	
	private void btnOnline() {
		btnOnline = new JButton("Listar Online");
		btnOnline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Collection<Repository> listaR = null;
				Collection<Client> listaC = null;
				
				try {
					listaR =  data.getRepositoriesSessions();
					listaC = data.getClientSessions(); 
					System.out.println(listaR.size() + " y el de clientes " + listaC.size());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
				String[] columnNames = {"Tipo","ID","Sesion","Nombre"};
				DefaultTableModel model = new DefaultTableModel(new Object[0][0],columnNames);
				
				for(Repository r : listaR) {
					Object[] row = new Object[4];
					row[0] = "Repositorio";
					row[1] = r.getId();
					row[2] = r.getSession();
					row[3] = r.getUser();
					model.addRow(row);
				}
				
				for(Client c : listaC) {
					Object[] row = new Object[4];
					row[0] = "Cliente";
					row[1] = c.getId();
					row[2] = c.getSession();
					row[3] = c.getUser();
					model.addRow(row);
				}
				
				table.setModel(model);
				table.updateUI();
			}
		});
	}
	
	private void exitButton() {
		btnExit = new JButton("Salir");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Naming.unbind("rmi://localhost:" + PORT + "/data");
					System.out.println("Servicio datos cerrado.");
					Naming.unbind("rmi://localhost:" + PORT + "/reg");
					System.out.println("Servicio registro cerrado.");
					Naming.unbind("rmi://localhost:" + PORT + "/autentication");
					System.out.println("Servicio autenticacion cerrado.");
					Naming.unbind("rmi://localhost:" + PORT + "/manager");
					System.out.println("Servicio gestor cerrado.");
					System.exit(0);
				} catch (RemoteException | MalformedURLException | NotBoundException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
