package remote.services.impl.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import remote.services.common.Fichero;
import remote.services.common.ServicioClOperadorInterface;
import remote.services.common.ServicioDatosInterface;

/**
 * Dado que los repositorios se crearan en la misma carpeta que el binario que contenga el main del repositorio,
 * tendremos que calcular en tiempo de ejecucion esa ruta para utilizarla a la hora del guardado.
 * @author ibernabeu
 *
 */
public class ServicioClOperadorImpl extends UnicastRemoteObject implements ServicioClOperadorInterface {

	private String classPath;
	
	public ServicioClOperadorImpl() throws RemoteException {
		super();
		//classPath = ServicioClOperadorImpl.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(5);
		//classPath = ServicioClOperadorImpl.class.getResourceAsStream("Repositorio.class").toString();
		//classPath = ServicioClOperadorImpl.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		classPath = ClassLoader.getSystemClassLoader().getResource(".").getPath();
	}

	@Override
	public boolean uploadFile(Fichero file) throws RemoteException {
		OutputStream os;
		File clientPath = new File(classPath + file.obtenerPropietario());
		if(!clientPath.exists())
			clientPath.mkdir();
		
		try {
			System.out.println("nobmre fichero" + file.obtenerNombre());
			os = new FileOutputStream(classPath + file.obtenerPropietario() + File.separator + file.obtenerNombre());
			file.escribirEn(os);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override 
	public boolean deleteFile(String fileName, int idClient) throws RemoteException {
		File filePath = new File(classPath + idClient + File.separator +  fileName);
		if(!filePath.exists())
			return false;
		return filePath.delete();
	}
}
