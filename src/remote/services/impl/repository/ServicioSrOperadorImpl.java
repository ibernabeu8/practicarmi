package remote.services.impl.repository;


import java.io.File;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import remote.services.common.Fichero;
import remote.services.common.ServicioDatosInterface;
import remote.services.common.ServicioDiscoClienteInterface;
import remote.services.common.ServicioSrOperadorInterface;
import remote.services.impl.server.Client;
import remote.services.impl.server.Metadata;
import remote.services.impl.server.Repository;
import remote.services.impl.test.ConstantLocalhost;

public class ServicioSrOperadorImpl extends UnicastRemoteObject implements ServicioSrOperadorInterface {

	private String classPath;
	private ServicioDiscoClienteInterface clientDisk;
	
	public ServicioSrOperadorImpl() throws RemoteException {
		super();
		//classPath = ServicioClOperadorImpl.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(5);
		//classPath = ServicioClOperadorImpl.class.getResourceAsStream("Repositorio.class").toString();
		classPath = ClassLoader.getSystemClassLoader().getResource(".").getPath();
	}


	/**
	 * @param El id del repositorio, que dara nombre a su carpeta.
	 */
	
	@Override
	public void newRepoClient(int idClient) throws RemoteException{
		File clientPath = new File(classPath + idClient);
		if(!clientPath.exists())
			clientPath.mkdir();
	}
	/*
	 * 	//classPath = ServicioClOperadorImpl.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(5) + idRepo + idFolder;
		//System.out.println("Ruta del cliente dentro del repo: " + classPath);
		//newFolder(classPath);(non-Javadoc)
	 * @see remote.services.common.ServicioSrOperadorInterface#downloadFile(java.lang.String)
	 */
	//desde el repo al cliente
	@Override
	public void downloadFile(String sessionClient, String fileName) throws RemoteException{
		try {
			ServicioDatosInterface data = (ServicioDatosInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			//Buscamos el id del cliente en el array de sesiones, para poder encontrar su carpeta en el repo
			Collection<Client> list = (Collection<Client>) data.getClientSessions();
			int propietario = list.stream()
								.filter(x -> x.getSession() == Integer.valueOf((sessionClient)))
								.findFirst()
								.get()
								.getId();
			String path = classPath + propietario + File.separator + fileName;
			//fin
			clientDisk = (ServicioDiscoClienteInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_CLIENTE + "/clientdisk/" + sessionClient);
			Fichero file = new Fichero(path,fileName,String.valueOf(propietario));
			clientDisk.downloadFileFromRepository(file);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch(NoSuchElementException  e) {
			System.out.println("no encentro el elemento");
		}
	}
	
	public Collection<Fichero> listadoClientesFicheros() {
		return null;
	} 
	/**
	 * Lista los clientes cuya sesion se corresponde con el repositorio.
	 * @param session
	 * @return
	 */
	public Collection<Client> listarClientes(int session) {
		try {
			ServicioDatosInterface data = (ServicioDatosInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			
			Map<Client,Repository> mapList = (Map<Client, Repository>) data.getClientsRepositoriesAsociate();
			Collection<Client> clientList = new ArrayList<Client>();
			
			int idRepo = ((Collection<Repository>)(data.getRepositoriesSessions()))
						.stream()
						.filter(x -> x.getSession() == session)
						.findFirst()
						.get()
						.getId();
			
			for(Map.Entry<Client, Repository> entry : mapList.entrySet()) {
				if(entry.getValue().getId() == idRepo) {
					clientList.add(entry.getKey());
				}
			}
			return clientList;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Recibe la session del repositorio y devuelve un listado con sus ficheros.
	 * @param session
	 * @return
	 */
	@Override
	public Collection<Fichero> getFiles(int session) throws RemoteException{
		ServicioDatosInterface data;
		try {
			data = (ServicioDatosInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			Collection<Repository> listRepository = (Collection<Repository>)data.getRepositoriesSessions();
			return listRepository.stream()
							.filter(x -> x.getSession() == session)
							.findFirst()
							.get().getFileList();
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		} catch(NoSuchElementException e) {
			System.out.println("No existen ficheros.");
		}
		return null;
	}
	@Override
	public Collection<Metadata> getFilesFromClient(int idClient) {
		ServicioDatosInterface data;
		Collection<Metadata> toReturnList = new ArrayList<Metadata>();
		
		try {
			data = (ServicioDatosInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			List<Metadata> filesInfo = (List<Metadata>) data.getMetadataInfo();
			int sessionRepo = data.searchRepoOfClient(idClient);
			int idRepo = ((Collection<Repository>) data.getRepositoriesSessions()).stream()
					.filter(x -> x.getSession() == sessionRepo)
					.findAny()
					.get()
					.getId();
			System.out.println("El id del reposiotior es: " + idRepo + "  el id del cliente es: " + idClient);
			for(Metadata m : filesInfo ) {
				if(m.getIdClient() == idClient && m.getIdRepository() == idRepo) {
					toReturnList.add(m);
				}
			}
		
		}catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		} catch(NoSuchElementException e) {
			System.out.println("No existen ficheros.");
		}
		
		return toReturnList;
	}
	
	@Override
	public void updateFilesList(int idClient, String fileName, int OPTION) {
		ServicioDatosInterface data;
		try {
			data = (ServicioDatosInterface) Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			int session = data.searchRepoOfClient(idClient);
			int idrepo =((Collection<Repository>) data.getRepositoriesSessions()).stream()
					.filter(x -> x.getSession() == session)
					.findAny()
					.get()
					.getId();
					
			if(OPTION == CREATE)
				data.updateMetadata(idrepo, idClient, fileName,CREATE);
			else
				data.updateMetadata(idrepo, idClient, fileName,DELETE);
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			
		}
		
		
	}
}
