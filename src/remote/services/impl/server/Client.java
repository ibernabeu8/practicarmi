package remote.services.impl.server;


/**
 * Entidad cuyos campos representan toda la informacion relativa a un cliente.
 * @author ibernabeu
 *
 */
public class Client extends SystemEntity {

	private static final long serialVersionUID = 1L;
	private Repository associatedRepository;
	
	public Client() {}
	
	public Client(String user, String password, Repository associateRepository) {
		super(user,password);
		associatedRepository = associateRepository;
		setOnline(false);
	}
	public Client(int id, String user, String password, Repository associateRepository) {
		super(id,user,password);
		associatedRepository = associateRepository;
		setOnline(false);
	}
	
	public Repository getAssociatedRepository() {
		return associatedRepository;
	}

	public void setAssociatedRepository(Repository associatedRepository) {
		this.associatedRepository = associatedRepository;
	}

}
