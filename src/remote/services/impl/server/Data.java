package remote.services.impl.server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * La entidad metadata se encargara de mantener la información referente a ficheros, clientes, repositoirios y todo tiempo información 
 * que registre els erivcio de datos.
 * @author ibernabeu
 *
 */
public class Data implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<Integer,Client> db_idClientMap = new HashMap<Integer,Client>();;
	
	private Map<Integer,Repository> db_idRepositoryMap = new HashMap<Integer,Repository>();
	
	private Map<Client,Repository> db_clientRepository = new HashMap<Client,Repository>();
	
	private List<Metadata> file_metadata = new ArrayList<Metadata>();
	
	public Data() {
		
	}
	
	public Map<Integer, Client> getDb_idClientMap() {
		return db_idClientMap;
	}
	
	public void addNewClient(int id, Client c) {
		db_idClientMap.put(id, c);
	}
	
	public void addNewRepository(int id, Repository r) {
		db_idRepositoryMap.put(id, r);
	}
	
	public void addNewClientRepository(Client c, Repository r) {
		db_clientRepository.put(c, r);
	}
	
	public void addNewFileData(Metadata m) {
		file_metadata.add(m);
	}
	
	public void deleteFileData(Metadata m) {
		file_metadata.remove(m);
	}
	
	
	public void setDb_idClientMap(Map<Integer, Client> db_idClientMap) {
		this.db_idClientMap = db_idClientMap;
		
	}

	public Map<Integer, Repository> getDb_idRepositoryMap() {
		return db_idRepositoryMap;
	}

	public void setDb_idRepositoryMap(Map<Integer, Repository> db_idRepositoryMap) {
		this.db_idRepositoryMap = db_idRepositoryMap;
	}

	public Map<Client, Repository> getDb_clientRepository() {
		return db_clientRepository;
	}

	public void setDb_clientRepository(Map<Client, Repository> db_clientRepository) {
		this.db_clientRepository = db_clientRepository;
	}
	
	public List<Metadata> getFileMetadata() {
		return this.file_metadata;
	}
	public int newIDRepo() {
		return db_idRepositoryMap.size() + 1;
	}
	public int newIDClient() {
		return db_idClientMap.size() + 1;
	}
}
