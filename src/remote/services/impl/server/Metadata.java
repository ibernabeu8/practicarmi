package remote.services.impl.server;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Esta clase contendra la informacion relacionada con cada fichero que se suba a un repositorio
 * @author ibernabeu
 *
 */
public class Metadata implements Serializable{
	
	private String fileName;
	private String date;
	private int idClient;
	private int idRepository;

	public Metadata(String fileName ,int idClient, int idRepository) {
		 LocalDate today = LocalDate.now();
		 LocalTime time = LocalTime.now();
		 date = (today + " " + time);
		 
		 this.fileName = fileName;
		 this.idClient = idClient;
		 this.idRepository = idRepository;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public int getIdRepository() {
		return idRepository;
	}
	public void setIdRepository(int idRepository) {
		this.idRepository = idRepository;
	}

	public String getDate() {
		return date;
	}
	
	@Override
	public String toString() {
		return "Fecha: " + date + " , " + fileName;
	}
}
