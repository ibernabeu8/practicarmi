package remote.services.impl.server;

import java.util.ArrayList;
import java.util.List;

import remote.services.common.Fichero;

/**
 * Entidad cuyos campos contendran informacion relevante a un repositorio.
 * @author ibernabeu
 *
 */
public class Repository extends SystemEntity {

	private static final long serialVersionUID = 1L;
	private List<Client> clientList;
	//private List<Fichero> fileList;
	private List<Fichero> fileList;
	
	public Repository(int lastID, String user, String pass) {
		super(lastID,user,pass);
		clientList = new ArrayList<Client>();
	}
/*
	public Repository(int lastID, String user, String pass, List<Client> clientList) {
		super(lastID,user,pass);
		this.clientList = clientList;
	}
*/
	public List<Client> getClientList() {
		return clientList;
	}
	public void setClientList(List<Client> clientList) {
		this.clientList = clientList;
	}
	
	public void addClientToList(Client c) {
		this.clientList.add(c);
	}
	
	public void addFileToList(Fichero f) {
		this.fileList.add(f);
	}
	
	public void removeClientFromList(Client c) {
		this.clientList.remove(c);
	}
	/*
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isOnline() {
		return isOnline;
	}
	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}*/

	public List<Fichero> getFileList() {
		return fileList;
	}

	public void setFileList(List<Fichero> fileList) {
		this.fileList = fileList;
	}
}
