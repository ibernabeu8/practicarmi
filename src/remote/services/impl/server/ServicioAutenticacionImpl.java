package remote.services.impl.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.rmi.server.UnicastRemoteObject;

import remote.services.common.ServicioAutenticacionInterface;
import remote.services.common.ServicioDatosInterface;
import remote.services.impl.test.ConstantLocalhost;
/**
 * Implementación del servicio de autenticación, se encargara de conectar los usuarios
 * al servicio de datos (el cual comprobara si estan registrados y OFFLINE) ,despues les asignara
 * una sesion , he utilizado la api UID que nos proporciona el propio java.rmi
 * Este genera un UID formado por 3 parametros, aunque yo utilizare el HASHCODE para simplificar
 * la practica.
 * @author ibernabeu
 *
 */
public class ServicioAutenticacionImpl extends UnicastRemoteObject implements ServicioAutenticacionInterface{

	private static final long serialVersionUID = 1L;
	private ServicioDatosInterface data;
	private int session;
	

	public ServicioAutenticacionImpl() throws RemoteException {
		super();
	}
	
	@Override
	public int validateClient(String name, String pass) throws RemoteException {
		System.out.println("ya estamos en el validador del client");
		session = getUID();
		
		try {
			data = (ServicioDatosInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data"); 
			System.out.println("Conectado con el servicio de datos desde autentication.");
			System.out.println("la pass en el autenticador es : " + pass);
			if(data.connect(session, name, pass, 1) == false)
				return 0;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return session;
	}

	@Override
	public int validateRepositorie(String name,  String pass) throws RemoteException {
		System.out.println("ya estamos en el validador del repo");
		session = getUID();
		
		try {
			data = (ServicioDatosInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			System.out.println("Conectado con el servicio de datos desde autentication.");
			if(data.connect(session, name, pass, 0) == false) {
				return 0;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return session;
	}

	@Override
	public void disconect(int session, int USER_TYPE) throws RemoteException {
		try {
			data = (ServicioDatosInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			System.out.println("Conectado con el servicio de datos desde autentication.");
			data.disconect(session, USER_TYPE);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	
	private int getUID() {
		return new UID().hashCode();
	}
}


