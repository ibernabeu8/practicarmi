package remote.services.impl.server;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;

import remote.services.common.Fichero;
import remote.services.common.ServicioDatosInterface;
import remote.services.common.ServicioSrOperadorInterface;
import remote.services.impl.test.ConstantLocalhost;

public class ServicioDatosImpl extends UnicastRemoteObject implements ServicioDatosInterface {

	private List<Client> clients_session = new ArrayList<Client>();
	private List<Repository> repositories_session = new ArrayList<Repository>();
	
	//private Collection<Client> db_client;
	//private Collection<Repository> db_repository;
	//private Collection<Repository> db_client_repository;
	
	private Data metadata;

	public ServicioDatosImpl() throws RemoteException {
		super();
		
		loadMD();
		
		if(metadata == null) {
			metadata = new Data();
		}
		
		Iterator<Metadata> it = metadata.getFileMetadata().iterator();
		
		while(it.hasNext()) {
			Metadata m = it.next();
			System.out.println(m.getFileName() + "  idC: " + m.getIdClient()  + " idR " + m.getIdRepository());
		}
	}


	
	@Override
	public boolean registrer(String user, String pass, int USER_TYPE) throws RemoteException {
		
		switch(USER_TYPE) {
		
		case USER_CLIENT:
				System.out.println("Registrando un cliente.");
				if(isClientRegistrer(user) == false) {
					int lastID = metadata.newIDClient();
					Client newClient = new Client(lastID,user,pass,null);
					metadata.addNewClient(lastID, newClient);
					System.out.println("El cliente: " + metadata.getDb_idClientMap().get(lastID).getUser() + " ha sido dado de alta.");
					asociateRepo(newClient);
					saveMD();
				}
				break;
		case USER_RESPOSITORY:
				System.out.println("Registrando un repositorio.");
				if(isRepositoryRegistrer(user) == false ) {
					int lastID = metadata.newIDRepo();
					Repository newRepository = new Repository(lastID,user,pass);
					metadata.addNewRepository(lastID, newRepository);
					System.out.println("El repositorio: " + metadata.getDb_idRepositoryMap().get(lastID).getUser() + " ha sido dado de alta.");
					saveMD();
				}
				break;
		default:
			return false;
		}
		return true;
	}

	@Override
	public boolean connect(int session, String user, String pass, int USER_TYPE) throws RemoteException {
		switch(USER_TYPE) {
		case USER_CLIENT:
			Client c = getClient(user);
			if(c != null && isLogedC(user) == false /*&& c.getPass().equals(pass)*/) {
				System.out.println("Cliente conectado con numero de sesion: " + session);
				c.setSession(session);
				clients_session.add(c);
				return true;
			}
			return false;
		case USER_RESPOSITORY:
			Repository r = getRepository(user);
			if(r != null && isLogedR(user) == false /*&& r.getPass().equals(pass)*/) {
				System.out.println("Repositorio conectado con numero de sesion:" + session);
				r.setSession(session);
				repositories_session.add(r);
				return true;
			}
			return false;
		default:
			System.out.println("No exsite el cliente o ya esta logueado.");
			return false;
		}
	}
	
	@Override
	public void disconect(int session, int USER_TYPE) {
		switch (USER_TYPE) {
		case USER_CLIENT:
			Client c = clients_session.stream()
							.filter(x -> x.getSession() == session)
							.findAny().get();
			clients_session.remove(c);
			break;
		case USER_RESPOSITORY:
			Repository r = repositories_session.stream()
						   .filter(x -> x.getSession() == session)
						   .findAny().get();
			repositories_session.remove(r);
			break;
		default:
			break;
		}
	}
	
	@Override 
	public Collection<Client> getClientSessions() throws RemoteException {
		return clients_session;
	}
	@Override 
	public Collection<Repository> getRepositoriesSessions() throws RemoteException{
		return repositories_session;
	}
	
	@Override
	public Collection<?> getMetadataInfo() throws RemoteException {
		return Collections.synchronizedList(metadata.getFileMetadata());
	}

	
	@Override
	public Collection<Client> getClients() throws RemoteException {
		return Collections.synchronizedMap(metadata.getDb_idClientMap()).values();
	}

	@Override
	public Collection<Repository> getRepositories() throws RemoteException {
		return Collections.synchronizedMap(metadata.getDb_idRepositoryMap()).values();
	}

	@Override
	public Map<Client,Repository> getClientsRepositoriesAsociate() throws RemoteException {
		return Collections.synchronizedMap(metadata.getDb_clientRepository());
	}
	/**
	 * devuelve la sesion del repositirio, no el ID
	 */
	@Override
	public int searchRepoOfClient(int idClient) throws RemoteException {
		Client c = getClient(idClient);
		return Collections.synchronizedMap(metadata.getDb_clientRepository()).get(c).getSession();
	}

	/*
	@Override
	public void updateFileListRepository(int idRepo, Fichero newfile) throws RemoteException {
		//buscamos el repo
		Repository r = getRepositories().stream().filter(x -> x.getId() == idRepo).findFirst().get();
		r.addFileToList(newfile);
		//Acutalizamos ese repositorio en los diferentes mapas
		metadata.getDb_idRepositoryMap().replace(r.getId(), r);
		//obtenemos el cliente , que viene ya en al info del fichero (el id)
		int idClient = Integer.valueOf(newfile.obtenerPropietario());
		//Actualizamos el par Cliente repo
		metadata.getDb_clientRepository().replace(getClient(idClient), r);
		saveMD();
	}*/
	@Override 
	public void updateMetadata(int idRepo, int idClient, String filename, int OPTION) {
		
		 if(OPTION == 1) {
			 Metadata m = new Metadata(filename,idClient,idRepo);
			 metadata.addNewFileData(m);
		 }
		 else {
			 System.out.println("ESTOY EN EL SERVIDOR LOS DATOS SON: "+  idRepo  + "   "  + idClient + "  " + filename);
			 Metadata m = metadata.getFileMetadata()
			 .stream()
			 .filter(x -> x.getIdClient() == idClient && x.getIdRepository() == idRepo && x.getFileName().equals(filename))
			 .findAny()
			 .get();
			 metadata.deleteFileData(m);
		 }
			 
		 saveMD();
	}
	
	//metodos privados
	private void loadMD() {
		String path = ClassLoader.getSystemClassLoader().getResource(".").getPath() + "data.dat";
		path = path.substring(5, path.length());
		System.out.println("Ruta carga: " + path);
		
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
			Data md = (Data) ois.readObject();
			metadata = md;
			ois.close();
		}catch(FileNotFoundException f){
			System.out.println("Archivo no encontrado, se generara uno cuando se den de alta nuevos usuarios/repositorios." );
		}catch(ClassNotFoundException e){
			e.printStackTrace();		
		}catch (EOFException e) {
			System.out.println("Final de archivo.");
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void saveMD() {
		
		String path = ClassLoader.getSystemClassLoader().getResource(".").getPath() + "data.dat";
		path = path.substring(5, path.length());
		System.out.println("Ruta guardado: " + path);
		generatePath(path);
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
			oos.writeObject(metadata);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		loadMD();
	}
	
	private boolean isClientRegistrer(String user) throws RemoteException {
		System.out.println("Comprobando registro.");
		return getClients().stream()
				  .filter(x -> x.getUser().toUpperCase().equals(user.toUpperCase()))
				  .findAny().isPresent();
	}
	
	private boolean isRepositoryRegistrer(String repository) throws RemoteException {
		return getRepositories().stream()
				  .filter(x -> x.getUser().toUpperCase().equals(repository.toUpperCase()))
				  .findAny().isPresent();
	}
	
	private Client getClient(String user) throws RemoteException {
		System.out.println("Buscando cliente");
		try {
			return getClients().stream()
					  .filter(x -> x.getUser().toUpperCase().equals(user.toUpperCase()))
					  .findFirst().get();
		}catch(NoSuchElementException e) {
			System.out.println("El cliente no se encuentra en la base de datos");
			return null;
		}

	}
	
	private Client getClientSession(int idsession) throws RemoteException {
		return clients_session.stream()
				  .filter(x -> x.getSession() == idsession)
				  .findFirst().get();
	}
	
	private Client getClient(int id) throws RemoteException {
		return getClients().stream()
				  .filter(x -> x.getId() == id)
				  .findFirst().get();
	}
	
	private Repository getRepository(String user) throws RemoteException {
		try {
			return getRepositories().stream()
				  	.filter(x -> x.getUser().toUpperCase().equals(user.toUpperCase()))
				  	.findFirst().get();
		}catch(NoSuchElementException e) {
			System.out.println("El cliente no se encuentra en la base de datos");
			return null;
		}
	}
	
	private boolean isLogedC(String user) throws RemoteException {
		return clients_session.stream()
				  .filter(x -> x.getUser().toUpperCase().equals(user.toUpperCase()))
				  .findAny().isPresent();
	}
	
	private boolean isLogedR(String user) throws RemoteException {
		return repositories_session.stream()
				  .filter(x -> x.getUser().toUpperCase().equals(user.toUpperCase()))
				  .findAny().isPresent();
	}
	
	private void generatePath(String path) {
		File file = new File(path);
		
		if(!file.exists()) {
			try {
				file.createNewFile();
				System.out.println("Se ha creado un fichero data.dat");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
//METODO DE MIERDA HAY QUE REESCRIBIRLO
	private void asociateRepo(Client c) throws RemoteException {
		Repository r;
		Random randomGenerator = new Random();
		System.out.println("Asociando un repositorio al nuevo cliente.");
		boolean hasRepo = metadata.getDb_clientRepository().get(c) != null ? true : false;
		System.out.println("HAS REPO: " + hasRepo);
		System.out.println("Vacios? " + Collections.synchronizedList(repositories_session).isEmpty());
		//searchRepoService();
		//Comprobamos que no tiene un repositorio asigando y que existen repositorios online para asignarle
		if( !hasRepo && !repositories_session.isEmpty()) {
			System.out.println("Buscando un repositorio online..");
			//Intentamos asignarle uno oline
			int index = randomGenerator.nextInt(repositories_session.size());
			r = repositories_session.get(index);
			r.addClientToList(c);
			metadata.addNewClientRepository(c, r);
		} else if( !hasRepo) { //buscamos uno en la bbdd ya que no existren oine
			System.out.println("Buscando un repositorio en la base de datos..");
			int index = randomGenerator.nextInt(metadata.getDb_idRepositoryMap().size());
			r = (Repository) (metadata.getDb_idRepositoryMap().values().toArray())[index];
			r.addClientToList(c);
			metadata.addNewClientRepository(c, r);
		} else {
			System.out.println("El servidor no puede asignarle un repositorio, registre uno o intentelo más tarde.");
		}
	}




	/*
	public static void main(String[] args) throws RemoteException {
		ServicioDatosImpl datos = new ServicioDatosImpl();
	}*/
}
