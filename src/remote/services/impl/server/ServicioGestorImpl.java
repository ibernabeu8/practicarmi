package remote.services.impl.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import remote.services.common.ServicioDatosInterface;
import remote.services.common.ServicioGestorInterface;
import remote.services.impl.test.ConstantLocalhost;
/**
 * Debido a que he decicido separar los IDs de la bbdd que identifica como un unico elemento una entidad en el sistema aqui he implementaod un sistmea de sesiones para las ursl
 * @author ibernabeu
 *
 */
public class ServicioGestorImpl extends UnicastRemoteObject implements ServicioGestorInterface {
	
	private ServicioDatosInterface data;
	
	public ServicioGestorImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param sesion del cliente (online).
	 * @return url del repositorio del cliente, con el id al inicio para crear la carpeta, no es la mejor solucion, pero temporalmente lo hare asi.
	 */
	@Override
	public String getURLClientOperator(int sessionClient) throws RemoteException {
		try {
			data = (ServicioDatosInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			List<Client> client_sessions = (List<Client>) data.getClientSessions();
			int idClient = client_sessions.stream().filter(x -> x.getSession() == sessionClient).findFirst().get().getId();
			int sessionRepository = data.searchRepoOfClient(idClient);
			return idClient + "-rmi://localhost:" + ConstantLocalhost.PUERTO_REPOSITORIO + "/clientOperator/" + sessionRepository;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @param url del repositorio del cliente para acceder al servicio SERVEROPERATOR añadiendole al principio el id del cliente que lo llama
	 */
	@Override
	public String getURLServerOperator(int sessionClient) throws RemoteException {
		try {
			data = (ServicioDatosInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
			List<Client> client_sessions = (List<Client>) data.getClientSessions();
			int idClient = client_sessions.stream().filter(x -> x.getSession() == sessionClient).findFirst().get().getId();
			int sessionRepository = data.searchRepoOfClient(idClient);
			return idClient + "-rmi://localhost:" + ConstantLocalhost.PUERTO_REPOSITORIO + "/serverOperator/" + sessionRepository;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} 
		return null;
	}

}
