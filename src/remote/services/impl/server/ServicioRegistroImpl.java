package remote.services.impl.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import remote.services.common.ServicioDatosInterface;
import remote.services.common.ServicioRegistroInterface;
import remote.services.impl.test.ConstantLocalhost;

public class ServicioRegistroImpl extends UnicastRemoteObject implements ServicioRegistroInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ServicioDatosInterface data;
	
	public ServicioRegistroImpl() throws RemoteException {
		super();
	}
	
	@Override
	public boolean regClient(String name, String pass) throws RemoteException {
		try {
			System.out.println("BUSCANDO SERVICIO DATOS PARA REGISTRAR.CLIENTE " + data.USER_CLIENT);
			data = (ServicioDatosInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR +"/data");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return data.registrer(name, pass, data.USER_CLIENT);
	}

	@Override
	public boolean regRepository(String name, String pass) throws RemoteException {
		try {
			System.out.println("BUSCANDO SERVICIO DATOS PARA REGISTRAR.REPO" + data.USER_RESPOSITORY);
			data = (ServicioDatosInterface)Naming.lookup("rmi://localhost:" + ConstantLocalhost.PUERTO_SERVIDOR + "/data");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return data.registrer(name, pass, data.USER_RESPOSITORY);
	}

}
