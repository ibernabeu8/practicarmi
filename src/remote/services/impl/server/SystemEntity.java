package remote.services.impl.server;

import java.io.Serializable;

public class SystemEntity implements Serializable{


	private static final long serialVersionUID = 1L;
	private int id;
	private int session;
	private String user;
	private String pass;
	private boolean isOnline;
	
	
	public SystemEntity() {}
	
	public SystemEntity(int id, String name, String pass) {
		this.id = id;
		this.user = name;
		this.pass = pass;
	}
	
	public SystemEntity(String name, String pass) {
		this.user = name;
		this.pass = pass;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String name) {
		this.user = name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public boolean isOnline() {
		return isOnline;
	}
	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

	public int getSession() {
		return session;
	}

	public void setSession(int session) {
		this.session = session;
	}
	@Override
	public String toString() {
		return this.user;
	}
}
